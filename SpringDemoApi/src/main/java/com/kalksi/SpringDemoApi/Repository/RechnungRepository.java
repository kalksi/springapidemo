package com.kalksi.SpringDemoApi.Repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kalksi.SpringDemoApi.Modells.Rechnung;


@Repository
public interface RechnungRepository extends CrudRepository<Rechnung, Long> {

}
