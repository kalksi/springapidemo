package com.kalksi.SpringDemoApi.Repository;



import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kalksi.SpringDemoApi.Modells.Items;


@Repository
public interface ItemsRepository extends CrudRepository<Items, Long> {

}
