package com.kalksi.SpringDemoApi.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kalksi.SpringDemoApi.Modells.Tisch;
@Repository
public interface TischRepository extends CrudRepository<Tisch, Integer> {

}
