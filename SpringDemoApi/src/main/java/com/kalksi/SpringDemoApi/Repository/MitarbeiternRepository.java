package com.kalksi.SpringDemoApi.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kalksi.SpringDemoApi.Modells.Mitarbeitern;


@Repository
public interface MitarbeiternRepository extends CrudRepository<Mitarbeitern, Integer>{

}
