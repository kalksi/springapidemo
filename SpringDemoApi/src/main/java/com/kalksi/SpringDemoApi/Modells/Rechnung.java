package com.kalksi.SpringDemoApi.Modells;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Rechnung {
	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@JsonFormat(pattern = "dd.MM.yyyy")
	@Basic
	private java.sql.Date datum;
	@Basic
	private java.sql.Time zeit;



	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Collection<Items> items = new ArrayList<>();

	@OneToOne
	private Tisch tisch;
	@OneToOne
	private Mitarbeitern mitarbeiter;



	public Collection<Items> getItems() {
		return items;
	}

	public void setItems(Collection<Items> items) {
		this.items = items;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Rechnung() {
		super();
	}


	public java.sql.Date getDatum() {
		return datum;
	}

	public void setDatum(java.sql.Date datum) {
		this.datum = datum;
	}

	public java.sql.Time getZeit() {
		return zeit;
	}

	public void setZeit(java.sql.Time sqlTime) {
		this.zeit = sqlTime;
	}



	public Tisch getTisch() {
		return tisch;
	}

	public void setTisch(Tisch tisch) {
		this.tisch = tisch;
	}

	public Mitarbeitern getMitarbeiter() {
		return mitarbeiter;
	}

	public void setMitarbeiter(Mitarbeitern mitarbeiter) {
		this.mitarbeiter = mitarbeiter;
	}

	@Override
	public String toString() {
		return "Rechnung [id=" + id + ", datum=" + datum + ", zeit=" + zeit + ", items=" + items + ", tisch=" + tisch
				+ ", mitarbeiter=" + mitarbeiter + "]";
	}

}
