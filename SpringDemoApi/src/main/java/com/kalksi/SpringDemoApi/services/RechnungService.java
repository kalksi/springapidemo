package com.kalksi.SpringDemoApi.services;



import java.util.Collection;

import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.kalksi.SpringDemoApi.Modells.Items;
import com.kalksi.SpringDemoApi.Modells.Rechnung;

import com.kalksi.SpringDemoApi.Repository.ItemsRepository;
import com.kalksi.SpringDemoApi.Repository.MitarbeiternRepository;
import com.kalksi.SpringDemoApi.Repository.RechnungRepository;
import com.kalksi.SpringDemoApi.Repository.TischRepository;


@Service
public class RechnungService {

	@Autowired
	private RechnungRepository rechnungRepository;
	
	@Autowired
	private ItemsRepository itemsRepository;
	
	@Autowired
	private MitarbeiternRepository mitarbeiternRepo;
	
	@Autowired
	private TischRepository tischRepo;

	public Rechnung rechnungSpeichern(Rechnung rechnung) {
		
		Rechnung neueRechnung = new Rechnung();
		//neueRechnung.setId(rechnung.getId());
		neueRechnung.setDatum(rechnung.getDatum());
		neueRechnung.setZeit(rechnung.getZeit());
		neueRechnung.setTisch( tischRepo.findById(rechnung.getTisch().getId() ).get()  );
		neueRechnung.setMitarbeiter(mitarbeiternRepo.findById(rechnung.getMitarbeiter().getId()).get());
		neueRechnung.getItems().addAll(
				rechnung.getItems().stream().map(i -> {
			Items items = itemsRepository.findById(i.getId()).get();
			return items;
		}).collect(Collectors.toList()));
		
		System.out.println(neueRechnung);
		
		return rechnungRepository.save(neueRechnung);

	}

}
